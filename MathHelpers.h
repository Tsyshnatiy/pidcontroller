#ifndef MATHHELPERS_H
#define MATHHELPERS_H

#define min(a,b) a > b ? b : a
#define max(a,b) a > b ? a : b

#endif // MATHHELPERS_H
