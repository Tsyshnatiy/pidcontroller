#include "PidController.h"

PidController::PidController()
{
    pidModel = 0;
}

void PidController::setPidParams(const PidSettings* const settings)
{
    if (!pidModel)
    {
        pidModel = new PidModel(settings);
    }
    else
    {
        pidModel->setKp(settings->kp);
        pidModel->setKi(settings->ki);
        pidModel->setKd(settings->kd);
        pidModel->setSetPoint(settings->setpoint);
        pidModel->setOutputLimits(settings->outMin, settings->outMax);
    }
}

double PidController::getNextPidValue(const double input)
{
    return pidModel->compute(input);
}

PidController::~PidController()
{
    if (pidModel)
    {
        delete pidModel;
    }
}
