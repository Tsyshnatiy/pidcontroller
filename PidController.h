#ifndef PIDCONTROLLER_H
#define PIDCONTROLLER_H

#include "PidTypes.h"
#include "PidModel.h"

class PidController
{
public:
    PidController();
    virtual ~PidController();

    void setPidParams(const PidSettings* const settings);
    double getNextPidValue(const double input);

private:
    PidModel* pidModel;
};

#endif // PIDCONTROLLER_H
