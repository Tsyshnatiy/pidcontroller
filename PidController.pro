#-------------------------------------------------
#
# Project created by QtCreator 2014-07-03T00:45:24
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = PidController
TEMPLATE = app

SOURCES += main.cpp \
    PidMainWindow.cpp \
    PidModel.cpp \
    PidController.cpp

HEADERS += PidMainWindow.h \
    PidModel.h \
    PidTypes.h \
    PidController.h \
    TimeHelpers.h \
    MathHelpers.h

FORMS    += PidUiMainForm.ui
