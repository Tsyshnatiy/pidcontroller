#include "PidMainWindow.h"
#include "PidTypes.h"
#include <stdexcept>

namespace Ui
{

MainWindow::MainWindow(PidController* cntr)
{
    controller = cntr;
	setupUi(this);

    QObject::connect(horizontalScrollBar, SIGNAL(valueChanged(int)), this, SLOT(onScrollValueChanged(int)));
    QObject::connect(TargetValLEdit, SIGNAL(textChanged(QString)), this, SLOT(onInputChanged(QString)));
    QObject::connect(KpLEdit, SIGNAL(textChanged(QString)), this, SLOT(onInputChanged(QString)));
    QObject::connect(KiLEdit, SIGNAL(textChanged(QString)), this, SLOT(onInputChanged(QString)));
    QObject::connect(KdLEdit, SIGNAL(textChanged(QString)), this, SLOT(onInputChanged(QString)));
    QObject::connect(PidMaxValLEdit, SIGNAL(textChanged(QString)), this, SLOT(onInputChanged(QString)));
    QObject::connect(PidMinValLEdit, SIGNAL(textChanged(QString)), this, SLOT(onInputChanged(QString)));
}

void MainWindow::initPid()
{
    onScrollValueChanged(horizontalScrollBar->value());
}

void MainWindow::retranslateUi(QMainWindow *MainWindow)
{
    MainWindow->setWindowTitle(QApplication::translate("MainWindow", "PidUi", 0));
    KpLabel->setText(QApplication::translate("MainWindow", "Kp:", 0));
    KiLabel->setText(QApplication::translate("MainWindow", "Ki:", 0));
    KdLabel->setText(QApplication::translate("MainWindow", "Kd:", 0));
    KpLEdit->setText(QApplication::translate("MainWindow", "1", 0));
    KiLEdit->setText(QApplication::translate("MainWindow", "1", 0));
    KdLEdit->setText(QApplication::translate("MainWindow", "1", 0));
    PidMaxValLEdit->setText(QApplication::translate("MainWindow", "255", 0));
    PidMaxValLabel->setText(QApplication::translate("MainWindow", "Max value:", 0));
    PidMinValLabel->setText(QApplication::translate("MainWindow", "Min value:", 0));
    PidMinValLEdit->setText(QApplication::translate("MainWindow", "0", 0));
    TargetValLabel->setText(QApplication::translate("MainWindow", "Target value:", 0));
    TargetValLEdit->setText(QApplication::translate("MainWindow", "50", 0));
    AbsInputLabel->setText(QApplication::translate("MainWindow", "Absolute input value:", 0));
    ErrorLabel->setText(QApplication::translate("MainWindow", "Error:", 0));
    ErrorValLable->setText(QString());
    PidOutputLabel->setText(QApplication::translate("MainWindow", "PID output:", 0));
    PidOutputValLabel->setText(QString());
    InputValLabel->setText(QString());
} // retranslateUi

void MainWindow::setupUi(QMainWindow *MainWindow)
{
    if (MainWindow->objectName().isEmpty())
        MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
    MainWindow->resize(369, 256);
    centralwidget = new QWidget(MainWindow);
    centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
    KpLabel = new QLabel(centralwidget);
    KpLabel->setObjectName(QString::fromUtf8("KpLabel"));
    KpLabel->setGeometry(QRect(10, 120, 31, 17));
    KiLabel = new QLabel(centralwidget);
    KiLabel->setObjectName(QString::fromUtf8("KiLabel"));
    KiLabel->setGeometry(QRect(110, 120, 21, 17));
    KdLabel = new QLabel(centralwidget);
    KdLabel->setObjectName(QString::fromUtf8("KdLabel"));
    KdLabel->setGeometry(QRect(210, 120, 21, 17));
    KpLEdit = new QLineEdit(centralwidget);
    KpLEdit->setObjectName(QString::fromUtf8("KpLEdit"));
    KpLEdit->setGeometry(QRect(50, 120, 51, 23));
    KiLEdit = new QLineEdit(centralwidget);
    KiLEdit->setObjectName(QString::fromUtf8("KiLEdit"));
    KiLEdit->setGeometry(QRect(140, 120, 51, 23));
    KdLEdit = new QLineEdit(centralwidget);
    KdLEdit->setObjectName(QString::fromUtf8("KdLEdit"));
    KdLEdit->setGeometry(QRect(240, 120, 51, 23));
    PidMaxValLEdit = new QLineEdit(centralwidget);
    PidMaxValLEdit->setObjectName(QString::fromUtf8("PidMaxValLEdit"));
    PidMaxValLEdit->setGeometry(QRect(120, 150, 51, 23));
    PidMaxValLabel = new QLabel(centralwidget);
    PidMaxValLabel->setObjectName(QString::fromUtf8("PidMaxValLabel"));
    PidMaxValLabel->setGeometry(QRect(10, 150, 81, 17));
    PidMinValLabel = new QLabel(centralwidget);
    PidMinValLabel->setObjectName(QString::fromUtf8("PidMinValLabel"));
    PidMinValLabel->setGeometry(QRect(10, 180, 67, 17));
    PidMinValLEdit = new QLineEdit(centralwidget);
    PidMinValLEdit->setObjectName(QString::fromUtf8("PidMinValLEdit"));
    PidMinValLEdit->setGeometry(QRect(120, 180, 51, 23));
    TargetValLabel = new QLabel(centralwidget);
    TargetValLabel->setObjectName(QString::fromUtf8("TargetValLabel"));
    TargetValLabel->setGeometry(QRect(10, 210, 91, 17));
    TargetValLEdit = new QLineEdit(centralwidget);
    TargetValLEdit->setObjectName(QString::fromUtf8("TargetValLEdit"));
    TargetValLEdit->setGeometry(QRect(120, 210, 51, 23));
    horizontalScrollBar = new QScrollBar(centralwidget);
    horizontalScrollBar->setObjectName(QString::fromUtf8("horizontalScrollBar"));
    horizontalScrollBar->setGeometry(QRect(10, 40, 351, 16));
    horizontalScrollBar->setPageStep(1);
    horizontalScrollBar->setValue(0);
    horizontalScrollBar->setSliderPosition(0);
    horizontalScrollBar->setOrientation(Qt::Horizontal);
    AbsInputLabel = new QLabel(centralwidget);
    AbsInputLabel->setObjectName(QString::fromUtf8("AbsInputLabel"));
    AbsInputLabel->setGeometry(QRect(10, 10, 151, 17));
    ErrorLabel = new QLabel(centralwidget);
    ErrorLabel->setObjectName(QString::fromUtf8("ErrorLabel"));
    ErrorLabel->setGeometry(QRect(10, 70, 67, 17));
    ErrorValLable = new QLabel(centralwidget);
    ErrorValLable->setObjectName(QString::fromUtf8("ErrorValLable"));
    ErrorValLable->setGeometry(QRect(60, 70, 67, 17));
    PidOutputLabel = new QLabel(centralwidget);
    PidOutputLabel->setObjectName(QString::fromUtf8("PidOutputLabel"));
    PidOutputLabel->setGeometry(QRect(200, 70, 81, 17));
    PidOutputValLabel = new QLabel(centralwidget);
    PidOutputValLabel->setObjectName(QString::fromUtf8("PidOutputValLabel"));
    PidOutputValLabel->setGeometry(QRect(280, 70, 67, 17));
    InputValLabel = new QLabel(centralwidget);
    InputValLabel->setObjectName(QString::fromUtf8("InputValLabel"));
    InputValLabel->setGeometry(QRect(170, 10, 81, 17));
    MainWindow->setCentralWidget(centralwidget);
    statusbar = new QStatusBar(MainWindow);
    statusbar->setObjectName(QString::fromUtf8("statusbar"));
    MainWindow->setStatusBar(statusbar);

    retranslateUi(MainWindow);

    QMetaObject::connectSlotsByName(MainWindow);
} // setupUi

void MainWindow::onScrollValueChanged(int newVal)
{
    QString txt;
    txt.setNum(newVal);
    InputValLabel->setText(txt);

    onInputChanged();
}

void MainWindow::onInputChanged(QString newVal)
{
    const double setPt = TargetValLEdit->text().toDouble();
    const double kp = KpLEdit->text().toDouble();
    const double ki = KiLEdit->text().toDouble();
    const double kd = KdLEdit->text().toDouble();
    const double min = PidMinValLEdit->text().toDouble();
    const double max = PidMaxValLEdit->text().toDouble();

    const PidSettings* const settings = new PidSettings(setPt, kp, ki, kd, min, max);

    try
    {
        controller->setPidParams(settings);
        double nextPidValue = controller->getNextPidValue(horizontalScrollBar->value());
        PidOutputValLabel->setText(QString::number(nextPidValue, 'g', 3));
    }
    catch(const std::out_of_range& oor)
    {
        PidOutputValLabel->setText("Min > max");
    }
}

} //namespace Ui
