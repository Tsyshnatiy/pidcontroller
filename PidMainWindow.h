/********************************************************************************
** Form generated from reading UI file 'PidUiMainForm.ui'
**
** Created: Thu Jul 3 00:29:22 2014
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef PIDMAINWINDOW_H
#define PIDMAINWINDOW_H

#include <QtCore/QVariant>
#include <QAction>
#include <QApplication>
#include <QButtonGroup>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QMainWindow>
#include <QScrollBar>
#include <QStatusBar>
#include <QWidget>
#include "PidTypes.h"
#include "PidController.h"

QT_BEGIN_NAMESPACE

namespace Ui 
{
	class MainWindow : public QMainWindow
	{
		Q_OBJECT
	public:
        explicit MainWindow(PidController* cntr);
        void initPid();
    public slots:
        void onScrollValueChanged(int newVal);
        void onInputChanged(QString newVal = "");
    private:
        PidController* controller;
	private:
        QWidget *centralwidget;

        //readonly labels
        QLabel *KpLabel;
        QLabel *KiLabel;
        QLabel *KdLabel;
        QLabel *PidOutputLabel;
        QLabel *PidMaxValLabel;
        QLabel *PidMinValLabel;
        QLabel *TargetValLabel;
        QStatusBar *statusbar;
        QLabel *AbsInputLabel;
        QLabel *ErrorLabel;

        // Readonly output labels
        QLabel *ErrorValLable;
        QLabel *PidOutputValLabel;
        QLabel *InputValLabel;

        // Input parameters
        QLineEdit *KpLEdit;
        QLineEdit *KiLEdit;
        QLineEdit *KdLEdit;
        QLineEdit *PidMaxValLEdit;
        QLineEdit *PidMinValLEdit;
        QLineEdit *TargetValLEdit;
        QScrollBar *horizontalScrollBar;

    private:
		void retranslateUi(QMainWindow *MainWindow);
		void setupUi(QMainWindow *MainWindow);
	};
} // namespace Ui

QT_END_NAMESPACE

#endif // PIDMAINWINDOW_H
