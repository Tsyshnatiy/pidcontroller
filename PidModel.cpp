#include <unistd.h>
#include <sys/time.h>
#include <math.h>
#include "PidModel.h"
#include "TimeHelpers.h"
#include "MathHelpers.h"

PidModel::PidModel(const PidSettings * const settings)
{
    this->setpoint = settings->setpoint;

    setOutputLimits(settings->outMin, settings->outMax);
    setTunings(settings->kp, settings->ki, settings->kd);
    integral = 0;
    struct timeval tv;
    gettimeofday(&tv, NULL);
    lastTime = us_to_s(tv.tv_usec); //microseconds
}

double PidModel::compute(const double input)
{
    //usleep is needed to have timePassed non zero
    usleep(1000);

    struct timeval tv;
    gettimeofday(&tv, NULL);
    double now = us_to_s(tv.tv_usec);
    double timePassed = now - lastTime;
    lastTime = now;

    /*Compute all the working error variables*/
    double error = setpoint - input;

    //smooth input.
    double dInput = (input - lastInput) / SMOOTH_COEFF;
    integral += timePassed * error; //rectangles

    /*Compute PID Output*/
    double output = kp * error + ki * integral + kd * dInput / timePassed;

    output = min(output, outMax);
    output = max(output, outMin);

    /*Remember some variables for next time*/
    lastInput = input;
    return output;
}

void PidModel::setTunings(double kp, double ki, double kd)
{
    this->kp = kp;
    this->ki = ki;
    this->kd = kd;
}

void PidModel::setOutputLimits(const double min, const double max)
{
    if(min >= max)
    {
        throw std::out_of_range("Min > max");
    }

    outMin = min;
    outMax = max;
}
