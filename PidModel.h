#ifndef PID_MODEL_H
#define PID_MODEL_H

#include "PidTypes.h"
#include <stdexcept>

#define SMOOTH_COEFF 1000.0f

class PidModel
{
public:
    explicit PidModel(const PidSettings * const settings);

public:
    double compute(const double input);

    void setOutputLimits(const double min, const double max);
    void setTunings(const double kp, const double ki, const double kd);
    void setSetPoint(const double setpoint) { this->setpoint = setpoint; }
    void setKp(const double kp) { this->kp = kp; }
    void setKi(const double ki) { this->ki = ki; }
    void setKd(const double kd) { this->kd = kd; }

private:
    double kp;
    double ki;
    double kd;

    double integral;
    double setpoint;

    double lastTime;
    double lastInput;

    double outMin;
    double outMax;
};
#endif //PID_MODEL_H

