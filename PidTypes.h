#ifndef PIDTYPES_H
#define PIDTYPES_H

struct PidSettings
{
    const double setpoint;
    const double kp;
    const double ki;
    const double kd;
    const double outMin;
    const double outMax;

    PidSettings(const double setPt,
                const double kP, const double kI, const double kD,
                const double outMinLim, const double outMaxLim)
        : setpoint(setPt),
          kp(kP), ki(kI), kd(kD),
          outMin(outMinLim), outMax(outMaxLim) {}
};

#endif // PIDTYPES_H
