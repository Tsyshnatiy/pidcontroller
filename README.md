# README #

This software implements PID control.
User acts as sensor: he moves scroll bar and watches for PID controller output.
Scroll bar can be replaced with real sensor (encoder for example) and you can pick PID coefficients with the help of this program.