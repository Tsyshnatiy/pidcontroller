#include <QApplication>
#include "PidMainWindow.h"
#include "PidController.h"
#include <QtCore>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PidController controller;
    Ui::MainWindow window(&controller);
    window.initPid();
    window.show();
    
    return a.exec();
}
